import axios, { AxiosStatic } from 'axios'

export default class AdminService {
  public static http: AxiosStatic = AdminService.setupAxios()

  static setupAxios(): AxiosStatic {
    // Create Axios Instance
    const axiosStatic = axios

    // Create interceptor for 401 requests
    axiosStatic.interceptors.response.use(
      (response) => response,
      (error) => {
        const status = error.response ? error.response.status : null
        if (status === 401) {
          // ... Handle unauthorized
        }
        return Promise.reject(error)
      }
    )
    return axiosStatic
  }
}
