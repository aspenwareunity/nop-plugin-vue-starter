interface GenericResponse {
  wasSuccessful: boolean
  message: string
}
export default GenericResponse
