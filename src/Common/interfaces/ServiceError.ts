export interface ServiceError {
  error: object
  flag: boolean
  generalError: string
}
