import 'core-js/stable'
import Vue from 'vue'
import Vuelidate from 'vuelidate'
import Admin from '@/Apps/Admin.vue'
import '@/Plugins/axios'
import vuetify from '@/Plugins/vuetify'

Vue.config.productionTip = false

Vue.use(Vuelidate)

new Vue({
  vuetify,
  inject: {
    theme: {
      default: { isDark: false },
    },
  },
  render: (h) => h(Admin),
}).$mount('#app')
