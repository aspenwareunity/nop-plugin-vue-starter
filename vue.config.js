// vue.config.js
const path = require('path')

module.exports = {
  outputDir: '../dist',
  filenameHashing: false,
  // delete HTML related webpack plugins
  chainWebpack: (config) => {
    config.devtool('source-map')

    // Add entry point for admin page
    config.entry('admin-manage').add('/src/admin.ts').end()

    config.resolve.alias.set('@', path.resolve(__dirname, '/src'))
  },
  transpileDependencies: ['vuetify', /@koumoul/],
}