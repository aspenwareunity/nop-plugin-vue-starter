module.exports = {
  printWidth: 120,
  tabWidth: 2,
  useTabs: false,
  singleQuote: true,
  printWidth: 120,
  noEmptyRulesets: false,
  quoteProps: 'as-needed',
  tailingComma: 'all',
  bracketSpacing: true,
  jsxBracketSameLine: false,
  arrowParens: 'always',
  semi: false,
  overrides: [
    {
      files: ["*.cs"],
      options: {
        semi: true,
        tabWidth: 4,
      }
    }
  ],
}
